﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF___DotNet_Traject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            Cursist Ann = new Cursist();
            Ann.Achternaam = "Dingske";
            Ann.Voornaam = "Ann";
            Ann.Avatar = new BitmapImage(new Uri("/Image/Ann.jpg", UriKind.Relative));
            Cursist vreemdeMan = new Cursist();
            vreemdeMan.Achternaam = "Kwaad";
            vreemdeMan.Voornaam = "Boze Said";
            vreemdeMan.Avatar = new BitmapImage(new Uri("/Image/Said2.jpg", UriKind.Relative));
            Cursist juisteSaid = new Cursist();
            juisteSaid.Achternaam = "Juiste";
            juisteSaid.Voornaam = "Normale Said";
            juisteSaid.Avatar = new BitmapImage(new Uri("/Image/Said1.jpg", UriKind.Relative));
            ListHardcodedLeerlingen.Items.Add(Ann);
            ListHardcodedLeerlingen.Items.Add(vreemdeMan);
            ListHardcodedLeerlingen.Items.Add(juisteSaid);
        }

        private void btnOpslaan_Click(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch
            { }
        }
   
        private void AfbeeldingZoeken()
        {

        }
        private void PersoonBestaatCheck()
        {

        }
    }
}
