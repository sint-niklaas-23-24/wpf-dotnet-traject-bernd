﻿using System;
using System.Windows.Media;

namespace WPF___DotNet_Traject
{
    class Werknemer : Persoon
    {
        private Guid _werknemernummer;
        private int _loon;
        public Werknemer() : base()
        {
            _werknemernummer = Guid.NewGuid();
        }
        public Werknemer(int loon, string eenAchternaam, string eenVoornaam, ImageSource eenAvatar) : base ( eenAchternaam, eenVoornaam, eenAvatar)
        {
            _werknemernummer = Guid.NewGuid();
            _loon = loon;
        }
        public Guid Werknemernummer
        {
            get
            {
                return _werknemernummer;
            }

        }
        public int loon
        {
            get { return _loon; }
            set {_loon = value; }
        }
        public override string Details()
        { return ""; }
    }
}
