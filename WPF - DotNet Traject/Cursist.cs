﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace WPF___DotNet_Traject
{
    class Cursist : Persoon
    {
        private Guid _cursistennummer;
        public Cursist() : base() 
        {
        _cursistennummer = Guid.NewGuid();
        }
        public Cursist(string eenAchternaam, string eenVoornaam, ImageSource eenAvatar) : base ( eenAchternaam, eenVoornaam, eenAvatar)
        {
            _cursistennummer = Guid.NewGuid();
        }
        public Guid Cursistennummer
        {
            get { return _cursistennummer; }
        }
        public override string Details()
        { return ""; }
    }
}
