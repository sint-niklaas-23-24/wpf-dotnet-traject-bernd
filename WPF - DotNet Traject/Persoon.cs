﻿using System;
using System.Linq;
using System.Windows.Media;

namespace WPF___DotNet_Traject
{
    class Persoon
    {
        //atributen
        private string _achternaam;
        private string _voornaam;
        private ImageSource _avatar;
        //constructor
        public Persoon() { }
        public Persoon(string eenAchternaam, string eenVoornaam, ImageSource eenAvatar)
        {
            Achternaam = eenAchternaam;
            Voornaam = eenVoornaam;
            Avatar = eenAvatar;   
        }

        //properties
        public string Achternaam
        {
            get { return _achternaam; }
            set
            {
                _achternaam = value;
                if (String.IsNullOrWhiteSpace(value))
                {
                    throw new Exception("Gelieve de achternaam juist in te vullen.");
                }
                else if (value.Any(char.IsDigit))
                {
                    throw new Exception("Er kunnen geen cijfers in een achternaam staan.");
                }
            }
        }
        public string Voornaam
        {
            get { return _voornaam; }
            set
            {
                _voornaam = value;
                if (String.IsNullOrWhiteSpace(value))
                {
                    throw new Exception("Gelieve de voornaam juist in te vullen.");
                }
                else if (value.Any(char.IsDigit))
                {
                    throw new Exception("Er kunnen geen cijfers in een voornaam staan.");
                }
            }

        }
        public virtual ImageSource Avatar
        {
            get { return _avatar; }
            set { _avatar = value; }
        }
        //methoden
        public virtual string Details()
        {
            return "";
        }
    }
}
